package com.matteobad.tictactoe;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private TicTacToe game;

    public void dropIn(View view) {
        ImageView move = (ImageView) view;
        this.game.makeMove(move);

        if (this.game.isGameOver()) {
            GridLayout gridLayout = (GridLayout) findViewById(R.id.gridLayout);
            TextView player1Victories = (TextView) findViewById(R.id.player1TextView);
            TextView player2Victories = (TextView) findViewById(R.id.player2TextView);
            player1Victories.setText(String.valueOf(this.game.getPlayer1Victories()));
            player2Victories.setText(String.valueOf(this.game.getPlayer2Victories()));
            this.game.resetGame(gridLayout);
        }


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.game = new TicTacToe(this);
    }
}
