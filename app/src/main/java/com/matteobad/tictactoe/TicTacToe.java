package com.matteobad.tictactoe;


import android.util.Log;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.Toast;

public class TicTacToe {

    /**
     * Fare una mossa
     *    - Capire se ha vinto
     *    si: aggiornare il punteggio, resettare
     *    no: niente
     *
     */

    /**
     * Winning counter.
     */
    private int[] victories;

    /**
     * Flag for terminated match.
     */
    private boolean gameOver = false;

    /**
     * 0 == player1
     * 1 == player2
     */
    int activePlayer = 0;

    /**
     * 0 means player1
     * 1 means player2
     * 2 means empty slot
     */
    private int[] state = { 2, 2, 2, 2, 2, 2, 2, 2, 2 };

    /**
     * Winning combinations.
     */

    /**
     * [
     *      [0,0,0],
     *      [0,0,0],
     *      [0,0,0]
     * ]
     *
     *
     */
    private int[][] winningCombinations = {
            {0,1,2}, {3,4,5}, {6,7,8},  // horizontal
            {0,3,6}, {1,4,7}, {2,5,8},  // vertical
            {0,4,8}, {2,4,6}            // diagonal
    };

    private MainActivity activity;

    public TicTacToe(MainActivity activity) {
        this.activity = activity;
        this.victories = new int[2];
        this.victories[0] = 0;
        this.victories[1] = 0;
    }

    public int getPlayer1Victories() {
        return this.victories[0];
    }

    public int getPlayer2Victories() {
        return this.victories[1];
    }

    public void makeMove(ImageView move) {
        int tag = Integer.valueOf(move.getTag().toString());

        if (this.activePlayer == 0 && this.state[tag] == 2) {
            this.state[tag] = 0;
            move.setImageResource(R.drawable.cross);
        }
        else if (this.activePlayer == 1 && this.state[tag] == 2) {
            this.state[tag] = 1;
            move.setImageResource(R.drawable.circle);
        }

        // Check if someone has won
        for (int[] triple: this.winningCombinations) {
            if (
                    this.state[triple[0]] == this.state[triple[1]] &&
                    this.state[triple[1]] == this.state[triple[2]] &&
                    this.state[triple[0]] != 2)
            {
                String winner = (this.activePlayer != 0) ? "Circle" : "Cross";

                this.victories[this.activePlayer]++;
                this.gameOver = true;
                Log.i("winner", winner);
                Toast.makeText(this.activity, winner, Toast.LENGTH_SHORT).show();
            }
        }

        // pareggio check
        boolean ciSonoPostiLiberi = false;
        for (int i = 0; i < this.state.length; i++) {
            if (this.state[i] == 2) {
                ciSonoPostiLiberi = true;
            }
        }

        if (!ciSonoPostiLiberi) {
            this.gameOver = true;
            Toast.makeText(this.activity, "Pareggio", Toast.LENGTH_SHORT).show();
        }


        this.activePlayer = (this.activePlayer == 0) ? 1 : 0;


    }

    public boolean isGameOver() {
        return this.gameOver;
    }

    /**
     * Reset match field.
     */
    public void resetGame(GridLayout gridLayout) {
        this.gameOver = false;
        this.activePlayer = 0;
        for (int i = 0; i < gridLayout.getChildCount(); i++) {
            ((ImageView) gridLayout.getChildAt(i)).setImageResource(0);
            this.state[i] = 2;
        }
    }

}
